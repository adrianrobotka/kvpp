#include "Config.hpp"
#include "ConfigParser.hpp"
#include <exception>

namespace kvpp {
    Config Config::parse(std::istream &stream) {
        return parser::parse(stream);
    }

    void Config::addPair(const config_val_t &key, const config_key_t &value) {
        storage.emplace(key, value);
    }

    std::ostream &operator<<(std::ostream &os, const Config &config) {
        os << '{';
        bool first = true;
        for (const auto &it : config.storage) {
            if (!first)
                os << ',';
            os << '"' << it.first << "\":\"" << it.second << '"';
            first = false;
        }
        os << '}';

        return os;
    }

    Config Config::parse(const char *file_name) {
        std::ifstream file(file_name);

        if (!file.is_open())
            throw std::runtime_error("Config file not found.");

        return parse(file);
    }

    size_t Config::getSize() const {
        return storage.size();
    }
}
