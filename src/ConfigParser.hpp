/**
 * @file
 * @brief Config parser
 *
 * Parser of an input stream
 *
 */

#ifndef PARSER_HPP
#define PARSER_HPP

#include "Config.hpp"

namespace kvpp {
    namespace parser {
        /**
         * Parses a stream
         * @param stream Input stream
         * @return Parsed config
         */
        Config parse(std::istream &stream);
    }
}

#endif //PARSER_HPP
