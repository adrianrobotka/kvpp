/**
 * @file
 * @brief Test file
 *
 */

#include <iostream>

#include "Config.hpp"
#include "ConfigException.hpp"

#include "memtrace.h"
#include "gtest_lite.h"

using kvpp::Config;
using kvpp::ConfigException;

/**
 * Test main
 * @return exit code
 */
int main() {
    GTINIT(std::cin);

    TEST(Test10, Sanity)
        EXPECT_THROW(Config::parse("not_existing_file.cfg"), std::runtime_error);
    }

    TEST(Test20, Parsing)
        EXPECT_THROW(Config::parse("bad_key.cfg"), kvpp::InvalidConfigKeyNameException);
        EXPECT_THROW(Config::parse("bad_value.cfg"), kvpp::InvalidConfigValueException);

        try {
            Config::parse("bad_key.cfg");
        }
        catch (kvpp::InvalidConfigKeyNameException &e) {
            EXPECT_EQ(e.getLineNumber(), (size_t) 2);
        }

        try {
            Config::parse("bad_value.cfg");
        }
        catch (kvpp::InvalidConfigValueException &e) {
            EXPECT_EQ(e.getLineNumber(), (size_t) 3);
        }
    }

    TEST(Test30, Official_example)
        EXPECT_NO_THROW(
                Config config = Config::parse("task.cfg");
                EXPECT_EQ((int) config.getSize(), 3);

                std::cout << config << std::endl;

                std::string copyright;
                int valtozok;
                bool megoldhato;

                config.getValue("copyright", copyright)
                        .getValue("valtozok", valtozok)
                        .getValue("megoldhato", megoldhato);

                EXPECT_EQ("BME, Budapest", copyright);
                EXPECT_EQ(2, (int) valtozok);
                EXPECT_EQ(false, megoldhato);
        );
    }

    TEST(Test40, Custom_example)
        EXPECT_NO_THROW(
                Config config = Config::parse("basic.cfg");
                EXPECT_EQ((int) config.getSize(), 3);

                std::cout << config << std::endl;

                std::string val;
                int n;

                config.getValue("val", val)
                        .getValue("n", n);

                EXPECT_EQ("ami", val);
                EXPECT_EQ(42, (int) n);
        );
    }

    GTEND(std::cerr);

    return 0;
}