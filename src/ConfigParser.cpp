/**
 * @file
 * @brief Config parser implementation
 *
 */

#include "ConfigParser.hpp"
#include "ConfigException.hpp"

namespace kvpp {
    namespace parser {
        /**
         * Possible states of config parsing
         */
        typedef enum config_parse_state {
            PRE_ENTRY, /** Before a line starts */
            READ_KEY, /** Read value of a key */
            END_OF_KEYNAME, /** Read key-value separator */
            READ_VALUE, /** Read a config value */
            END_OF_VALUE, /** Key-value pair is ready */
            COMMENT, /** Read a comment line */
        } ParseState;

        /**
         * Determinates the next state of the state-machine
         * @param state Current state
         * @param c Processed character
         * @return The next state
         */
        ParseState getNextState(const ParseState state, const char c) {
            switch (state) {
                case PRE_ENTRY:
                case END_OF_VALUE:
                    if (c == '#')
                        return COMMENT;
                    else if (c == '\n' || c == ' ')
                        return PRE_ENTRY;
                    else
                        return READ_KEY;
                case READ_KEY:
                    return c == '=' ? END_OF_KEYNAME : READ_KEY;
                case COMMENT:
                    return c == '\n' ? PRE_ENTRY : COMMENT;
                case END_OF_KEYNAME:
                    return READ_VALUE;
                case READ_VALUE:
                    return c == '\n' ? END_OF_VALUE : READ_VALUE;
                default:
                    throw InvalidConfigParseStateException();
            }
        }

        /**
         * Check validity of the key
         * @param stream Stream of the key
         * @return validity
         */
        bool isKeyValid(std::stringstream &stream);

        /**
         * Check validity of the value
         * @param stream Stream of the value
         * @return validity
         */
        bool isValueValid(std::stringstream &stream);

        /**
         * Remove "/' characters at the beginning/ending from a _valid_ value field
         * @param stream
         * @return
         */
        std::string clearValue(std::string stream);

        Config parse(std::istream &stream) {
            Config config;

            ParseState state = PRE_ENTRY; // initial state
            char c; // current character of the processing
            size_t line = 1; // line counter
            std::stringstream key, value; // buffer
            while (!stream.eof()) {
                stream.get(c);

                try {
                    state = getNextState(state, c);
                } catch (InvalidConfigParseStateException &e) {
                    e.setLineNumber(line);
                    throw e;
                }

                switch (state) {
                    case PRE_ENTRY:
                        break; // waiting for the next char
                    case READ_KEY:
                        key << c;
                        break;
                    case END_OF_KEYNAME:
                        if (!isKeyValid(key))
                            throw InvalidConfigKeyNameException(key.str(), line);
                        break;
                    case READ_VALUE:
                        value << c;
                        break;
                    case COMMENT: // ignore comment
                        break;
                    case END_OF_VALUE:
                        if (!isKeyValid(key))
                            throw InvalidConfigKeyNameException(key.str(), line);
                        if (!isValueValid(value))
                            throw InvalidConfigValueException(key.str(), value.str(), line);
                        config.addPair(key.str(), clearValue(value.str())); // store pair
                        key = std::stringstream();
                        value = std::stringstream();
                        break;
                    default:
                        throw InvalidConfigParseStateException(line);
                }

                if (c == '\n')
                    line++;
            }
            return config;
        }

        bool isKeyValid(std::stringstream &stream) {
            char c;
            while (stream >> c) {
                if (!isalnum(c))
                    return false;
            }
            return true;
        }

        bool isValueValid(std::stringstream &stream) {
            char c;
            stream >> c;
            if (c == '"') {
                while (stream >> c) {
                    // no validation
                }
                return c == '"';
            } else if (c == '\'') {
                while (stream >> c) {
                    // no validation
                }
                return c == '\'';
            } else {
                while (stream >> c) {
                    if (!isalnum(c)) {
                        return false;
                    }
                }
                return true;
            }
        }

        std::string clearValue(std::string string) {
            if (string[0] == '"' || string[0] == '\'')
                return string.substr(1, string.length() - 2);

            return string;
        }
    }
}