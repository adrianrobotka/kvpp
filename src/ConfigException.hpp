/**
 * @file
 * @brief Config exceptions
 *
 * Exceptions in connection with the config
 *
 */

#ifndef CONFIG_EXCEPTION_HPP
#define CONFIG_EXCEPTION_HPP

#include <exception>

namespace kvpp {
    /**
     * Exception in connection with the config
     */
    class ConfigException : public std::runtime_error {
    protected:
        /**
         * Config file/stream line number
         */
        size_t lineNumber;

    public:
        /**
         * ConfigException ctor
         * @param lineNumber Config stream line number
         */
        explicit ConfigException(size_t lineNumber) : runtime_error("ConfigException"), lineNumber(lineNumber) {}

        /**
        * Say what...
        * @return Cause of the exception
        */
        const char *what() const noexcept override {
            return exception::what();
        }

        /**
         * Get line number
         * @return Line number
         */
        size_t getLineNumber() const {
            return lineNumber;
        }
    };

    /**
     * Cannot parse the config file exception
     */
    class InvalidConfigParseStateException : public ConfigException {
    public:
        explicit InvalidConfigParseStateException() : ConfigException(0) {}

        /**
         * InvalidConfigParseStateException ctor
         * @param lineNumber Config stream line number
         */
        explicit InvalidConfigParseStateException(size_t lineNumber) : ConfigException(lineNumber) {}

        /**
         * Set the line number
         * @param line Config stream line number
         */
        void setLineNumber(size_t line) {
            lineNumber = line;
        }


        /**
        * Say what...
        * @return Cause of the exception
        */
        const char *what() const noexcept override {
            std::stringstream ss;
            ss << "Invalid config parsing state on line: ";
            ss << lineNumber;
            return ss.str().c_str();
        }
    };

    /**
     * Invalid name of the config entry exception
     */
    class InvalidConfigKeyNameException : public ConfigException {
    public:
        /**
         * Content of the key
         */
        std::string key;

        /**
         * InvalidConfigKeyNameException ctor
         * @param key Content of the key
         * @param lineNumber Config stream line number
         */
        explicit InvalidConfigKeyNameException(std::string key, size_t lineNumber) : ConfigException(lineNumber),
                                                                                     key(std::move(key)) {}

        /**
        * Say what...
        * @return Cause of the exception
        */
        const char *what() const noexcept override {
            std::stringstream ss;
            ss << "Invalid config key name \"";
            ss << key;
            ss << "\" on line: ";
            ss << lineNumber;
            return ss.str().c_str();
        }
    };

    /**
     * Invalid value of the config entry exception
     */
    class InvalidConfigValueException : public ConfigException {
    public:
        /**
         * Content of the key
         */
        std::string key;

        /**
         * Content of the value
         */
        std::string value;

        /**
         * InvalidConfigValueException ctor
         * @param key Content of the key
         * @param value Content of the value
         * @param lineNumber Config stream line number
         */
        explicit InvalidConfigValueException(std::string key, std::string value, size_t lineNumber) :
                ConfigException(lineNumber), key(key), value(value) {}

        /**
         * Say what...
         * @return Cause of the exception
         */
        const char *what() const noexcept override {
            std::stringstream ss;
            ss << "Invalid config value \"";
            ss << value;
            ss << "\" for key \"";
            ss << key;
            ss << "\" on line: ";
            ss << lineNumber;
            return ss.str().c_str();
        }
    };
}

#endif //CONFIG_EXCEPTION_HPP
