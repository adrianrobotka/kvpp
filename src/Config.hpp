/**
 * @file
 * @brief Config class.
 *
 * Simple key-value config storage
 *
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <map>
#include <sstream>
#include <fstream>
#include <ostream>

/**
 * Config key type
 */
typedef std::string config_key_t;

/**
 * Config value type
 */
typedef std::string config_val_t;

/**
 * Config storage type
 */
typedef std::map<config_key_t, config_val_t> storage_t;

namespace kvpp {

    /**
     * Config class.
     */
    class Config {
    private:
        /**
         * Key-value storage
         */
        storage_t storage;

    public:
        /**
         * Creates a config instance
         */
        Config() : storage(storage_t()) {}

        /**
         * Parses an input as config
         * @param stream config stream
         * @return Parsed config
         */
        static Config parse(std::istream &stream);

        /**
         * Parses an input as config
         * @param file_name config file
         * @return Parsed config
         */
        static Config parse(const char *file_name);

        /**
         * Add a key-value pair to the config
         * @param key Name of the config entry
         * @param value Value of the config entry
         */
        void addPair(const config_key_t &key, const config_val_t &value);

        /**
         * Get the number of the config entries
         * @return Number of the config entries
         */
        size_t getSize() const;

        /**
         * Get config value by key name
         * @tparam T Type of the value
         * @param key_name Name of the config entry
         * @param out The value of the entry
         * @return Config instance
         */
        template<class T>
        inline Config getValue(const config_key_t &key_name, T &out) {
            std::stringstream ss(storage[key_name]);
            ss >> out;
            return *this;
        }

        /**
         * Get config value by key name
         * @tparam T Type of the value
         * @param key_name Name of the config entry
         * @param out The value of the entry
         * @return Config instance
         */
        inline Config getValue(const config_key_t &key_name, config_val_t &out) {
            out = storage[key_name];
            return *this;
        }

        /**
         * JSON-like printer
         * @param os Stream to write on
         * @param config Config instance
         * @return Stream
         */
        friend std::ostream &operator<<(std::ostream &os, const Config &config);
    };
}

#endif //CONFIG_HPP
