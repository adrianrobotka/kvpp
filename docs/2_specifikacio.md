# Pontosított feladatspecifikáció
A feladatleírásnak megfelelő kulcs-érték tároló osztályt fogok 
megvalósítani. A konfigutációs állomány felolvasása után 
hozzáférhetőfé teszi annak feldolgozott tartalmát példány szinten.

Belső hibakezeléssel látom el, így jelezni tudja majd a 
feldolgozási problémákat.

A konfigurációs bejegyzéshez tartozó értéket általánosan teszem
elérhetővé, megkövetelve, hogy értelmezve legyen az 
operator>>(stringstream&, T&) művelet.

A példa fájlban demonstrálni fogom a konfigurációs osztály
teljes működését.
