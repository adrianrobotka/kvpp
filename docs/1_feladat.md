# Feladat: Konfiguráció beolvasó
Készítsen osztályt konfigurációs fájl beolvasásához!
Az osztály olvassa be a megadott fájlt, 
és az abban leírt kulcs-érték párokat tárolja el.

Az osztálynak legyen olyan generikus függvénye, 
amivel egy adott kulcshoz tartozó (sztringként tárolt) 
értéket vissza tud adni a generikus típusra (T) konvertálva. 

A konverzió egyszerűsítése érdekében megkövetelheti, 
hogy a generikus típusra értelmezett legyen az 
operator>>(stringstream&, T&) művelet. 
A konfigurációs fájl sorokból áll. 
Az egyes sorok szerkezetét a következő EBNF leírással adjuk meg:

```
 <sor>::= <üres>
         |<komment>
         |<kulcs>=<érték>
 <komment>:: # <tetszőleges_karaktersorozat>
 <kulcs>::= <betűvel_kezdődő_alfanumerikus_karaktersorozat>
 <érték>::= <szó>
           | "<tetszőleges_karaktersorozat>"
           | '<tetszőleges_karaktersorozat>'
 <szó>::= <alfanumerikus_karaktersorozat>
Példa konfigurációs fájlra:

# Világegyenlet-megoldó konfig
# a következő érték egész szám, a generikus fv. int-ként adja vissza:
valtozok=2

# karaktertömb, a generikus fv. C-sztring vagy std::string típusra konvertálja,
# attól függően, hogy a generikus fv. melyik példányát használták.
copyright="BME, Budapest"

# a következő bool típus:
megoldhato=false

# példa utolsó sora
```

Specifikáljon egy egyszerű tesztfeladatot, amiben fel tudja használni az elkészített adatszerkezetet! A tesztprogramot külön modulként fordított programmal oldja meg! A megoldáshoz felhasználhat STL tárolót, vagy algoritmust is!
