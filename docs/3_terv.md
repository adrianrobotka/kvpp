# Feladat
Készítsen osztályt konfigurációs fájl beolvasásához!
Az osztály olvassa be a megadott fájlt, 
és az abban leírt kulcs-érték párokat tárolja el.

Az osztálynak legyen olyan generikus függvénye, 
amivel egy adott kulcshoz tartozó (sztringként tárolt) 
értéket vissza tud adni a generikus típusra (T) konvertálva. 

A konverzió egyszerűsítése érdekében megkövetelheti, 
hogy a generikus típusra értelmezett legyen az 
operator>>(stringstream&, T&) művelet. 
A konfigurációs fájl sorokból áll. 
Az egyes sorok szerkezetét a következő EBNF leírással adjuk meg:

```
 <sor>::= <üres>
         |<komment>
         |<kulcs>=<érték>
 <komment>:: # <tetszőleges_karaktersorozat>
 <kulcs>::= <betűvel_kezdődő_alfanumerikus_karaktersorozat>
 <érték>::= <szó>
           | "<tetszőleges_karaktersorozat>"
           | '<tetszőleges_karaktersorozat>'
 <szó>::= <alfanumerikus_karaktersorozat>
Példa konfigurációs fájlra:

# Világegyenlet-megoldó konfig
# a következő érték egész szám, a generikus fv. int-ként adja vissza:
valtozok=2

# karaktertömb, a generikus fv. C-sztring vagy std::string típusra konvertálja,
# attól függően, hogy a generikus fv. melyik példányát használták.
copyright="BME, Budapest"

# a következő bool típus:
megoldhato=false

# példa utolsó sora
```

Specifikáljon egy egyszerű tesztfeladatot,
amiben fel tudja használni az elkészített adatszerkezetet!
A tesztprogramot külön modulként fordított programmal oldja meg!
A megoldáshoz felhasználhat STL tárolót, vagy algoritmust is!

# Feladatspecifikáció
A feladatleírásnak megfelelő kulcs-érték tároló osztályt fogok 
megvalósítani. A konfigutációs állomány felolvasása után 
hozzáférhetőfé teszi annak feldolgozott tartalmát példány szinten.

Belső hibakezeléssel látom el, így jelezni tudja majd a 
feldolgozási problémákat.

A konfigurációs bejegyzéshez tartozó értéket általánosan teszem
elérhetővé, megkövetelve, hogy értelmezve legyen az 
operator>>(stringstream&, T&) művelet.

A példa fájlban demonstrálni fogom a konfigurációs osztály
teljes működését.

# Pontosított feladatspecifikáció
Jelen pontosított specifikáció végleges, egyoldalúan nem módosítható.
Célom egy könnyen használható kofiguráció feldolgozó felületet biztosítani
a programozó számára. Ez további tervezést igényel.

# Terv
A feladat egy tesztprogramot és egy osztálydiagram megtervezését igényli.

## Tesztprogram
A következő kódminta szerint szeretném elérhetővé tenni a konfigurációt,
a konfigurációs fájl feldogozása után. Ez utóbbi műveletet a parse metódus
hajtja végre és kivételt dob hoba esetén.

```
Config config = Config::parse("app.cfg");
std::string str;
int n;
config.getValue("app_name", str);
config.getValue("port_num", n);
```

# Programlogika felépítése
A Config osztály egy `std::map<config_key_t, config_val_t>` tárolóban helyezi
el a kulcs-érték párokat, ahol a config_*_t egy `std::string`.

A Config osztály, mint intefész UML diagramja, T template paraméterrel:
```
+-----------------------------------------------------------+
|                      class: Config                        |
|-----------------------------------------------------------|
|  + static Config parse(const char *file_name)             |
|  + Config getValue(const config_key_t &key_name, T &out)  |
+-----------------------------------------------------------+
```

A parse metódus megnyitja a fájl, hiba esetén kivételt dob, majd a feldogozást
rábízza egy állapotgépre. Ez a következő állapotokkal rendelkezik:
```
PRE_ENTRY, /** Before a line starts */
READ_KEY, /** Read value of a key */
END_OF_KEYNAME, /** Read key-value separator */
READ_VALUE, /** Read a config value */
END_OF_VALUE, /** Key-value pair is ready */
COMMENT, /** Read a comment line */
```
Az állapotgép megkülönbözteti az állapotátmeneti logikát, mely az aktuális
állapotra és az éppen feldolgozandó karakterre támaszkodik, valamint az
állapotgép törzsét, mely felel a mellékhatásokért, a beolvasott adatok
összefűzéséért, azok ellenőrzéséért, illetve a kivételekért.

A következő kivételek lehetségesek, feldolgozás közben:
```
InvalidConfigParseStateException
InvalidConfigKeyNameException
InvalidConfigValueException
```
Ezek tartalmazzák, hogy hanyadik sor feldolgozása közben keletkeztek.
A formátum vizsgálat ellenőrzi, hogy csak alfanumerikus karaktereket
lehessen beolvasni.
